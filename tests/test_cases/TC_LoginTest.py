import unittest
from time import sleep        
from selenium.webdriver.common.by import By


class LoginTest(unittest.TestCase):

    #Constructor
    def __init__(self, testName, driver,url,username,pwd):
        unittest.TestCase.__init__(self,testName)  
        self.driver=driver
        self.url=url
        self.username=username
        self.pwd=pwd

    #Test for login using google signin
    def test_loginByEmail(self):
        try:
            self.driver.get(self.url)
            # storing the current window handle to get back to dashboard
            sleep(2)
            main_page = self.driver.current_window_handle
            self.driver.find_element("xpath","//span[@class='firebaseui-idp-text firebaseui-idp-text-long']").click()
            sleep(5)
            for handle in self.driver.window_handles:
                if handle != main_page:
                    login_page = handle
                    break
            # change the control to signin pop window
            self.driver.switch_to.window(login_page)
            self.driver.find_element("name","identifier").send_keys(self.username)
            nextButton = self.driver.find_elements("xpath","//span[normalize-space()='Next']")
            nextButton[0].click()
            sleep(5)
            passWordBox = self.driver.find_element("xpath","//input[@name='password']")
            passWordBox.send_keys(self.pwd)
            nextButton = self.driver.find_elements("xpath","//span[normalize-space()='Next']")
            nextButton[0].click()
            sleep(5)
            self.driver.switch_to.window(main_page)#move back from login to main page
            '''sleep(3)
            self.driver.find_element(By.XPATH,"//h6[normalize-space()='Application Form']").click()
            self.driver.find_element(By.CSS_SELECTOR,"label[for='declaration_1']").click()
            self.driver.find_element(By.CSS_SELECTOR,"label[for='declaration_2']").click()
            self.driver.find_element(By.XPATH,"//input[@id='next_']").click()'''
            print("Online degree Login with gmail account, Pass")
        except Exception as e:
            print("Online degree Login with gmail account, Fail\nError: "+str(e))
